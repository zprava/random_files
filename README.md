# random_files

Notes, presentations, or ideas to be shared with the group

- [configurations](./configurations/) - configuration files
- [shared-keys](./shared-keys/) - shared keys
- [documentation](./documentation/) - project documentation

[A nice reference about RESTful APIs](https://restful-api-design.readthedocs.io/en/latest/intro.html)

# About methods

- `GET` gets the contents of an existing resource (query)
- `POST` creates a new resource with a key the server decides on (create)
- `PUT` replaces the contents on an existing resource (modify)
- `PATCH` updates part of the contents on an existing resource (update)

# Authorization

Every request that requires authentication must first obtain [a token][token auth] via `/user/log_in POST`.
In every other request the HTTP Header `Authorization` should be included in the following form:

```text
Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b
```

[token auth]: https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication

# General response format

Roughly based off of: https://google.github.io/styleguide/jsoncstyleguide.xml

Dates should be in [ISO 8601][iso 8601] format.
An example date is `1985-04-12T23:20:50.52Z` which corresponds to April 12, 1985 23:20:50.52 UTC.
All times should be in UTC unless otherwise specified.

Rough example (N.B. [comments are not valid JSON!][json comments]):
```js
{
  "error": { // Optional - should only exist when an error has occured!
    "code": 404, // Error code for the client to handle known error conditions with (such as resource already exists)
    "message": "Resource not found" // Human readable error message to possibly show the user.
  }
  "data": ... // Payload goes here!
}
```

[iso 8601]: https://en.wikipedia.org/wiki/ISO_8601
[json comments]: https://stackoverflow.com/q/244777

# Push notifications

Simply add a `"device_id"` field to the `/user/log_in POST` and get notifications via Google (?).

# Errors



# The resources

*All resources require a session token (except for `/user/log_in POST` and `/user POST`)*

## Resource `/user`

There are no methods specified for `/user`.

### Call `/user/:id GET`

Get information about another user.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

URI: `/user/3`

(No body)

#### Response

```js
{
  "data": {
    "created": "2018-11-07T05:00:38.586000Z",
    "deleted": null,
    "email": "UserC@example.com",
    "first_name": "FirstC",
    "id": 3,
    "last_name": "LastC",
    "phone": "5555557777",
    "updated": "2018-11-13T06:27:12.859000Z",
    "username": "UserC"
  }
}
```

## Resource `/user/me`

Shortcut resource for the currently logged in user.

### Call `/user/me GET`

Get information about the currently logged in user.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

(No body)

#### Response

```js
{
  "data": {
    "created": "2018-11-07T05:00:38.586000Z",
    "deleted": null,
    "email": "UserC@example.com",
    "first_name": "FirstC",
    "id": 3,
    "last_name": "LastC",
    "phone": "5555557777",
    "updated": "2018-11-13T06:27:12.859000Z",
    "username": "UserC"
  }
}
```

### Call `/user/me PUT`

<!-- sanitize: updated -->

Replace the current logged in user's `user` object with a new one.
`password` field is optional, and will only be changed if included in the request.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

```js
{
  "created": "2018-11-07T05:00:38.586000Z",
  "deleted": null,
  "email": "UserC@example.com",
  "first_name": "NewFirst",
  "id": 3,
  "last_name": "NewLast",
  "phone": "5555551234",
  "updated": "2018-11-13T06:27:12.859000Z",
  "username": "UserC"
}
```

#### Response

```js
{
  "data": {
    "created": "2018-11-07T05:00:38.586000Z",
    "deleted": null,
    "email": "UserC@example.com",
    "first_name": "NewFirst",
    "id": 3,
    "last_name": "NewLast",
    "phone": "5555551234",
    "updated": "2018-11-13T06:27:12.859000Z",
    "username": "UserC"
  }
}
```

### Call `/user POST`

<!-- sanitize: created,updated,id -->

Register a new user.

#### Request

This request should never include `id`.

```js
{
  "username": "yourusername",
  "first_name": "Your first name",
  "last_name": "Your last name",
  "phone": "15555555", // Optional, can be null
  "email": "user@example.com",
  "password": "new user password"
}
```

#### Response

A newly created user.
To log in, make sure to call `/user/log_in POST`.

```js
{
  "data": {
    "username": "yourusername",
    "first_name": "Your first name",
    "last_name": "Your last name",
    "phone": "15555555", // Or null
    "email": "user@example.com",
    "id": 1,
    "created": "1985-04-12T23:20:50.52Z",
    "updated": "1985-04-12T23:20:50.52Z",
    "deleted": null
  }
}
```

### Call `/user/log_in POST`

<!-- sanitize: token -->

Log in.

#### Request

```js
{
  "username": "UserA",
  "password": "PasswordA",
  "device_id": "Your device ID" // Optional. For push notifications.
}
```

#### Response

```js
{
  "data": {
    "token": "the new authentication token"
  }
}
```

### Call `/user/log_out POST`

Log out. (Invalidates the session token.)

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

(No body)

#### Response

```js
{
  "data": {
    "logged_out": true
  }
}
```

## Resource `/conversation`

### Call `/conversation?active_since=<datetime> GET`

Obtain a list of conversations the logged in user is a participant in.
`?active_since=<datetime>` is optional, and limits the selection to conversations that had a message created/modified/deleted since `<datetime>`.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

URI: `/conversation`

(No body)

#### Response

```js
{
  "data": {
    "items": [
      {
        "created": "2018-11-13T06:25:29.213000Z",
        "deleted": null,
        "id": 4,
        "last_active": "2018-11-29T19:15:57.493000Z",
        "name": "Conversation 4",
        "updated": "2018-11-13T06:27:05.739000Z",
        "users": [ // Three users in this conversation
          {
            "id": 1,
            "username": "UserA"
          },
          {
            "id": 2,
            "username": "UserB"
          },
          {
            "id": 3,
            "username": "UserC"
          }
        ]
      }
      // ... and the rest of the conversations
    ]
  }
}
```

### Call `/conversation POST`

<!-- sanitize: created,updated,last_active,id -->

Create a new conversation.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

```js
{
  "name": "New name",
  "users": [
    {
      "id": 3 // Match by id (make sure to add your own user!)
    },
    {
      "username": "uSeRB" // Match by username. Case insensitive.
    }
  ]
}
```

#### Response

```js
{
  "data": {
    "id": 20, // Conversation ID
    "created": "1985-04-12T23:20:50.52Z",
    "updated": "1985-04-12T23:20:50.52Z",
    "deleted": null,
    "last_active": "1985-04-12T23:20:50.52Z",
    "name": "New name",
    "users": [
      {
        "id": 2,
        "username": "UserB"
      },
      {
        "id": 3,
        "username": "UserC"
      }
    ]
  }
}
```

### Call `/conversation/:id GET`

Get the specific conversation.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

URI: `/conversation/4`

(No body)

#### Response

```js
{
  "data": {
    "created": "2018-11-13T06:25:29.213000Z",
    "deleted": null,
    "id": 4,
    "last_active": "2018-11-29T19:15:57.493000Z",
    "name": "Conversation 4",
    "updated": "2018-11-13T06:27:05.739000Z",
    "users": [ // Three users in this conversation
      {
        "id": 1,
        "username": "UserA"
      },
      {
        "id": 2,
        "username": "UserB"
      },
      {
        "id": 3,
        "username": "UserC"
      }
    ]
  }
}
```

### Call `/conversation/:id PUT`

<!-- sanitize: last_active,updated -->

Modify the conversation (add/remove users).

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

Remove nick, phil, and add foo@example.com.

URI: `/conversation/4`

```js
{
  "created": "2018-11-13T06:25:29.213000Z",
  "deleted": null,
  "id": 4,
  "last_active": "2018-11-29T19:15:57.493000Z",
  "name": "cool kids", // change name
  "updated": "2018-11-13T06:27:05.739000Z",
  "users": [
    // Remove UserA
    {
      "username": "UserB"
    },
    {
      "id": 3 // UserC
    }
  ]
}
```

#### Response

```js
{
  "data": {
    "created": "2018-11-13T06:25:29.213000Z",
    "deleted": null,
    "id": 4,
    "last_active": "2018-11-29T19:15:57.493000Z",
    "name": "cool kids", // change name
    "updated": "2018-11-13T06:27:05.739000Z",
    "users": [
      // Remove UserA
      {
        "id": 2,
        "username": "UserB"
      },
      {
        "id": 3,
        "username": "UserC"
      }
    ]
  }
}
```

### Call `/conversation/:id/message/:mid GET`

Get a specific message

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

URI: `/conversation/4/message/5`

(No body)

#### Response


```js
{
  "data": {
    "content": "aGVsbG8gd29ybGQ=", // Base64 encoded "hello world"
    "conversation": 4, // Conversation ID
    "created": "2018-11-29T19:15:57.493000Z",
    "deleted": null,
    "id": 5, // Message ID
    "kind": "text/plain", // MIME of the content
    "sender": {
      "id": 3, // Sender User ID
      "username": "UserC"
    },
    "updated": "2018-11-29T19:15:57.493000Z"
  }
}
```

### Call `/conversation/:id/message?since=<datetime> GET`

Get all the messages for the conversation. `?since=...` is optional.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 UserC -->

URI: `/conversation/4/message`

(No body)

#### Response

```js
{
  "data": {
    "items": [
      {
        "content": "aGVsbG8gd29ybGQ=",
        "conversation": 4,
        "created": "2018-11-29T19:15:57.493000Z",
        "deleted": null,
        "id": 5,
        "kind": "text/plain",
        "sender": {
          "id": 3,
          "username": "UserC"
        },
        "updated": "2018-11-29T19:15:57.493000Z"
      }
      // ... The rest of the messages
    ]
  }
}
```

### Call `/conversation/:id/message POST`

<!-- sanitize: id,created,updated -->

Create message to conversation.

#### Request

<!-- auth: 5a7fe4aa4a62fef96ee9ed94b511b8b659563490 (for UserC) -->

URI: `/conversation/4/message`

```js
{
  "sender": { // Can also include "username" field
    "id": 3 // Your User ID
  },
  "conversation": 4, // Conversation ID
  "kind": "text/plain",
  "content": "Z29vZGJ5ZSB3b3JsZA==" // 'goodbye world' base64 encoded
}
```

#### Response


```js
{
  "data": {
    "id": 11, // Message ID
    "sender": {
      "id": 3,
      "username": "UserC"
    },
    "conversation": 4, // Conversation ID
    "kind": "text/plain",
    "content": "Z29vZGJ5ZSB3b3JsZA==", // 'goodbye world' base64 encoded
    "created": "1985-04-12T23:20:50.52Z",
    "updated": "1985-04-12T23:20:50.52Z",
    "deleted": null
  }
}
```

# Possible changes

- pagination
- resource deletion (delete user, conversation, message, etc)
- resource for streaming support (push notifications)
- resource(s) for discovery of other users / or contacts/favorites
- [specify how concurrent updates are handled][concurrent updates]
- Been asked this twice, maybe three times - why not use [ISO 8601][ISO 8601] dates, since they're simpler?
- Long term goal: support for encryption

[concurrent updates]: https://blog.4psa.com/rest-best-practices-managing-concurrent-updates/
[ISO 8601]: https://en.wikipedia.org/wiki/ISO_8601

## TODO

- No `"datum"` matching (client is intended to only add my username)
- No milliseconds on time (PLS)

# Run tests against this document

See [./doctest/README.md](this document).

This is a tree of the important files of our server stack.

Some directories are omitted, and this tree shouldn't be used verbatim or in-place.
Instead copy files over as needed.

Make no assumption this tree is up-to-date.

#!/bin/sh
set -eu
# Check if we are deploy, otherwise run this command as deploy.
if [ "$(whoami)" != deploy ]; then
    exec sudo -u deploy -i /srv/dc.sh "$@"  # FIXME change this line when file is moved.
fi
docker-compose \
	-p zprava \
	-f /srv/caddy/docker-compose.yml \
	-f /srv/zprava-api-service/docker-compose.prod.yml \
	"$@"
